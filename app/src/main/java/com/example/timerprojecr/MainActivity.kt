package com.example.timerprojecr

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import android.view.View
import android.widget.SeekBar
import android.widget.Toast
import androidx.core.app.NotificationCompat
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_main.*
import kotlin.math.abs

class MainActivity : AppCompatActivity() {
    lateinit var notificationManager: NotificationManager
    var play = true
    lateinit var times: SeekBar
    var secondss = 300
    lateinit var timer: CountDownTimer

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        times = findViewById(R.id.timeeditor)
        notificationManager = getSystemService(NotificationManager::class.java)
        times.max = 300000
        times.setOnSeekBarChangeListener(object: SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                var seconds : Int = (abs(progress-300000)/1000)
                val minutes : Int = seconds/60
                secondss=seconds
                seconds -= minutes * 60
                var str = "$minutes:"
                if (seconds < 10) {str+="0"}
                str+=seconds.toString()
                time.text = str
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {}

            override fun onStopTrackingTouch(seekBar: SeekBar?) {}

        })
    }

    fun onSomeClicked(v: View) {
        if (play) {
            timer = object: CountDownTimer((secondss*1000).toLong(), 1000) {
                override fun onTick(p0: Long) {
                    if (p0<=(secondss*1000).toLong()-1000 && !play)
                        times.progress = times.progress + 1000
                }

                override fun onFinish() {
                    if (!play) {
                        Snackbar.make(times, "Время вышло!", Snackbar.LENGTH_SHORT).show()
                        button.text = "Старт"
                        times.isEnabled = true
                        times.progress = 300000
                        sendNotification("Время вышло!", "Timer")
                        play = true
                    }
                }
            }
            timer.start()
            button.text = "Сбросить"
            sendNotification("Таймер запущен!", "Timer")
            times.isEnabled = false
            play=false
        } else {
            timer.cancel()
            button.text = "Старт"
            times.isEnabled = true
            play=true
        }
    }

    private fun createChannel(): Boolean {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val notificationChannel = NotificationChannel(
                    "321",
                    "channelName321",
                    NotificationManager.IMPORTANCE_HIGH
            )
                    .apply {
                        setShowBadge(false)
                    }

            notificationChannel.enableLights(true)
            notificationChannel.lightColor = Color.RED
            notificationChannel.enableVibration(true)
            notificationChannel.description = "Таймер"
            notificationManager.createNotificationChannel(notificationChannel)
            return true
        }
        return false
    }

    private fun sendNotification(message: String, title: String) {
        val contentIntent = Intent(applicationContext, MainActivity::class.java)
        val contentPendingIntent = PendingIntent.getActivity(
                this,
                321,
                contentIntent,
                PendingIntent.FLAG_ONE_SHOT
        )

        val builder = NotificationCompat.Builder(
            this,
                "321"
        )
                .setSmallIcon(R.drawable.cooked_egg)
                .setContentTitle(title)
                .setContentText(message)
                .setContentIntent(contentPendingIntent)
                .setAutoCancel(true)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
        if (createChannel()) {
            notificationManager.notify(321, builder.build())
        } else {
            Toast.makeText(this, "Ваша версия SDK устарела", Toast.LENGTH_SHORT).show()
        }
    }
}